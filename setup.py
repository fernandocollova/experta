from setuptools import setup

setup(
    name='matrix',
    version="0.0.1",
    description='Matrix transformation app',
    packages=[
        'matrix',
    ],
    scripts=[
        'matrix_app.py',
    ],
)
