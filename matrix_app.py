#!python

from matrix.argparse import MatrixArgParser
from matrix.interface import TextInterface as InterfacedMatrix

def main(instruction, size):

    interfaced_matrix = InterfacedMatrix(size)
    print(interfaced_matrix.execute(instruction))

if __name__ == "__main__":
    try:
        args_parser = MatrixArgParser()
        args = args_parser.parse_args()
        main(**vars(args))
    except KeyboardInterrupt:
        pass
