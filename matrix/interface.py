from matrix.matrix import Matrix, Interfaceable


class TextInterface(Matrix):

    def execute(self, instructions):
        """
        Return a string with the result of the operation, or "ERROR" on errors.

        Valid operations are Matrix methods decorated with the @Interfaceable
        decorator, in uppercase.
        """

        parts = instructions.split(" ")

        try:
            operation = parts.pop(0).lower()
            if operation not in Interfaceable.methods:
                return "ERROR"
            method = getattr(self, operation)
            result = method(*[int(number) for number in parts])
            if result:
                return "SUCCESS {}".format(result)
            return "SUCCESS"
        except:
            return "ERROR"
