from matrix.exceptions import OutOfRangeException

class Interfaceable():

    methods = []

    @classmethod
    def __call__(cls, func):
        cls.methods.append(func.__name__)
        return func

class Matrix():
    """
    A 3 dimensional matrix that can be updated and queried.

    The constructor accepts single argument, size, that can go from 1 to any
    positive integer. It's an error to create a 0 sized matrix.
    """

    def __init__(self, size):

        if size < 1:
            raise ValueError("The matrix size must be grater than 0")

        self.size = size
        self.values = [[size*[0, ] for _ in range(size)] for _ in range(size)]

    def check_boundries(self, x, y, z):
        """
        Raise OutOfRangeException if the coordinates are outside the
        matrix range
        """

        for coordinate in x, y, z:
            if coordinate > self.size - 1:
                raise OutOfRangeException(
                    "The query is outside the matix range"
                )

    @Interfaceable()
    def update(self, x, y, z, value):
        """Update the coordinates to the value"""

        self.check_boundries(x, y, z)
        self.values[x][y][z] = value

    @Interfaceable()
    def query(self, x1, y1, z1, x2, y2, z2):
        """Return the sum of all the values between the provided coordinates"""

        self.check_boundries(x1, y1, z1)
        self.check_boundries(x2, y2, z2)

        result = 0
        for x_range in range(x1, x2+1):
            for y_range in range(y1, y2+1):
                for cell in range(z1, z2+1):
                    result += self.values[x_range][y_range][cell]

        return result

    def __eq__(self, other):

        if isinstance(other, self.__class__):
            return self.values == other.values
        elif isinstance(other, list):
            return self.values == other
        else:
            return False

    def __str__(self):

        return str(self.values)

    def __repr__(self):

        return self.__str__()
