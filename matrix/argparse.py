from argparse import ArgumentParser

class MatrixArgParser(ArgumentParser):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.add_argument(
            '-e', '--execute',
            help="Execute the provided staement",
            type=str,
            dest='instruction',
            required=True,
        )

        self.add_argument(
            '-s', '--size',
            help="Size of the matrix to operate on",
            type=int,
            dest='size',
            required=True,
        )
