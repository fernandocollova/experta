import unittest
import matrix

class TestTextInterface(unittest.TestCase):

    def test_successful_update(self):

        expected_matrix = [
            [
                [0, 0, 0],
                [1, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
        ]

        reference_matrix = matrix.TextInterface(3)
        result = reference_matrix.execute("UPDATE 0 1 0 1")
        self.assertEqual(result, "SUCCESS")
        self.assertEqual(expected_matrix, reference_matrix.values)

    def test_successful_query(self):

        expected_matrix = matrix.TextInterface(3)
        expected_matrix.values = [
            [
                [0, 0, 0],
                [1, 0, 0],
                [1, 0, 0],
            ],

            [
                [0, 1, 0],
                [0, 0, 0],
                [1, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [1, 0, 0],
            ],
        ]

        x1, y1, z1 = 0, 0, 0
        x2, y2, z2 = 1, 1, 1

        result = expected_matrix.execute(
            "QUERY {} {} {} {} {} {}".format(x1, y1, z1, x2, y2, z2)
        )
        self.assertEqual(result, "SUCCESS 2")

    def test_out_of_range_fails_on_update(self):

        W = 2
        x, y, z = 0, 4, 0

        reference_matrix = matrix.TextInterface(3)
        result = reference_matrix.execute("UPDATE {} {} {} {}".format(x, y, z, W))
        self.assertEqual(result, "ERROR")


    def test_out_of_range_fails_on_query(self):

        x1, y1, z1 = 0, 0, 0
        x2, y2, z2 = 1, 5, 0

        reference_matrix = matrix.TextInterface(3)
        result = reference_matrix.execute(
            "QUERY {} {} {} {} {} {}".format(
                x1, y1, z1,
                x2, y2, z2,
            )
        )
        self.assertEqual(result, "ERROR")

    def test_unknown_operation_fails(self):

        x1, y1, z1 = 0, 0, 0
        x2, y2, z2 = 1, 5, 0

        reference_matrix = matrix.TextInterface(3)
        result = reference_matrix.execute(
            "JUMP {} {} {} {} {} {}".format(
                x1, y1, z1,
                x2, y2, z2,
            )
        )
        self.assertEqual(result, "ERROR")
