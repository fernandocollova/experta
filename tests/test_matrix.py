import unittest
from matrix import Matrix
from matrix.exceptions import OutOfRangeException

class TestMatrix(unittest.TestCase):

    def test_successful_update(self):

        expected_matrix = [
            [
                [0, 1, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
        ]

        W = 1
        x, y, z = 0, 0, 1

        reference_matrix = Matrix(size=3)
        reference_matrix.update(x, y, z, W)
        self.assertEqual(expected_matrix, reference_matrix.values)

    def test_comparision_with_lists(self):

        expected_matrix = [
            [
                [0, 0, 0],
                [1, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
        ]

        W = 1
        x, y, z = 0, 1, 0

        reference_matrix = Matrix(3)
        reference_matrix.update(x, y, z, W)
        self.assertEqual(expected_matrix, reference_matrix)

    def test_comparision_with_matrix_objects(self):

        W = 2
        x, y, z = 0, 1, 0

        reference_matrix = Matrix(3)
        reference_matrix.update(x, y, z, W)
        expected_matrix = Matrix(3)
        expected_matrix.update(x, y, z, W)

        self.assertEqual(expected_matrix, reference_matrix)

    def test_query(self):

        expected_matrix = Matrix(3)
        expected_matrix.values = [
            [
                [0, 0, 0],
                [1, 0, 0],
                [1, 0, 0],
            ],

            [
                [0, 1, 0],
                [0, 0, 0],
                [1, 0, 0],
            ],

            [
                [0, 0, 0],
                [0, 0, 0],
                [1, 0, 0],
            ],
        ]

        x1, y1, z1 = 0, 0, 0
        x2, y2, z2 = 1, 1, 1

        self.assertEqual(expected_matrix.query(x1, y1, z1, x2, y2, z2), 2)

    def test_out_of_range_exception_on_update(self):

        W = 2
        x, y, z = 0, 4, 0

        reference_matrix = Matrix(3)
        self.assertRaises(
            OutOfRangeException,
            reference_matrix.update,
            x, y, z,
            W
        )

    def test_out_of_range_exception_on_query(self):

        x1, y1, z1 = 0, 0, 0
        x2, y2, z2 = 1, 5, 0

        reference_matrix = Matrix(3)
        self.assertRaises(
            OutOfRangeException,
            reference_matrix.query,
            x1, y1, z1,
            x2, y2, z2,
        )

    def test_value_error_is_raised_on_negative_size(self):

        self.assertRaises(ValueError, Matrix, -1)

    # def test_numpy_rocks(self):
    #     """
    #     All this cloud be done with numpy, so i'm including this commented
    #     test just because numpy is awesome.
    #     """

    #     import numpy as np
    #     reference_matrix = np.zeros((2, 3, 4))

    #     expected_matrix = [
    #         [
    #             [1, 0, 0, 0],
    #             [0, 0, 0, 0],
    #             [0, 0, 0, 0],
    #         ],

    #         [
    #             [0, 0, 0, 0],
    #             [0, 0, 0, 0],
    #             [0, 0, 0, 0],
    #         ]
    #     ]

    #     W = 1
    #     x, y, z = 0, 0, 0

    #     updated_matrix = reference_matrix[x][y][z].update(W)
    #     self.assertEqual(expected_matrix, updated_matrix.tolist())
