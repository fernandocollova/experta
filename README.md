# Matrix operations app

The solution to the excercise proposed by `Experta ART`

## How to install it:

`pip3 install --user git+https://fernandocollova@bitbucket.org/fernandocollova/experta.git`

## How to use it:

Just run `matrix_app.py` followed by:

    -s [--size] and the size of the matrix
    -e [--execute] and the operation to perform to the matrix

## How to run the tests:

Clone this repo, cd to the top directory, and run `python3 -m unittest tests`
